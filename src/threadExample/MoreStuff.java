package threadExample;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;

public class MoreStuff implements Runnable {

    private AtomicInteger c = new AtomicInteger(0);
    Boolean tf = null;

    public MoreStuff(AtomicInteger x, Boolean y) {
        this.c = x;
        this.tf = y;
    }

    public void increment() {
        c.incrementAndGet();
       // c.incrementAndGet();
        System.out.println(c.toString());
    }

    public void decrement() {
        c.decrementAndGet();
        System.out.println(c.toString());
    }

    public int value() {
        return c.get();
    }


    @Override
    public void run() {
        if (tf) {
            for (int i = 0; i < 999 && i > -999; i++) {
                increment();
                System.out.println("adding");
            }

        } else if (!tf) {
            for (int i = 0; i < 999 && i > -999; i++) {
                decrement();
                System.out.println("subtracting");
            }
        }
    }
}
