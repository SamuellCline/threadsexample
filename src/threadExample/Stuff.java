package threadExample;

import java.util.Random;

public class Stuff implements Runnable {

    private String name;
    private int number;
    private int sleep;
    private int rand;

    public Stuff(String name, int number, int sleep) {





        this.name = name;
        this.number = number;
        this.sleep = sleep;

        Random random = new Random();
        this.rand = random.nextInt(1000);
    }

    public void run() {

        for (int count = 1; count < rand; count++) {
            if (count % number == 0) {
                System.out.print(name + " is running. " + "\n\n");
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n\n" + name + " is done.\n\n");
    }
}
