package threadExample;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class Start {
    public static void main(String[] args) {
    AtomicInteger x = new AtomicInteger(400);
        ExecutorService esvc1 = Executors.newFixedThreadPool(2);
        MoreStuff mst1 = new MoreStuff(x, true);
        MoreStuff mst2 = new MoreStuff(x, false);

        Stuff t1 = new Stuff("thread1", 25, 1000);
        Stuff t2 = new Stuff("thread2", 10, 500);
        Stuff t3 = new Stuff("thread3", 5, 250);
        Stuff t4 = new Stuff("thread4", 2, 100);
        Stuff t5 = new Stuff("thread5", 1, 50);
        Stuff t6 = new Stuff("thread6", 1, 50);

     //   esvc1.execute(t1);
      //  esvc1.execute(t2);
      //  esvc1.execute(t3);
      //  esvc1.execute(t4);
      //  esvc1.execute(t5);
       // esvc1.execute(t6);
        esvc1.execute(mst1);
        esvc1.execute(mst2);

        esvc1.shutdown();
    }
}
